import java.io.*;
import java.util.*;

class Solution3 {
	public static void main(String[] args) {
		List<Animal> animals = getAnimalList(); // list of random animals
		printGroupedZoo(animals);
	}

	/*
	 * Print out a list of all the animals, grouped by animal type sort by animal
	 * weight frog: { type: frog weight: 806 color: green id: 14} { type: frog
	 * weight: 382 color: red id: 9} buffalo: { type: buffalo weight: 575 color:
	 * orange id: 13} { type: buffalo weight: 558 color: red id: 4} . . .
	 */
	public static void printGroupedZoo(List<Animal> animals) {
		System.out.println("Zoo Animals:");
	}

	public static class Animal {
		private String type;
		private int weight;
		private String color;
		private int id;

		public Animal(String type, int weight, String color, int id) {
			this.type = type;
			this.weight = weight;
			this.color = color;
			this.id = id;
		}

		public String getType() {
			return this.type;
		}

		//In pounds 'lbs'
		public int getWeight() {
			return this.weight;
		}

		public String getColor() {
			return this.color;
		}

		public int getId() {
			return this.id;
		}
	}

	//Test Data
	private static final Random random = new Random(123);
	private static List<String> type = Arrays.asList("spider", "tiger", "panda", "penguin", "parrot", "buffalo",
			"frog");
	private static List<String> color = Arrays.asList("red", "green", "blue", "orange");

	private static String getRandomType() {
		return type.get(random.nextInt(type.size()));
	}

	private static int getRandomWeight() {
		return random.nextInt(1001);
	}

	private static String getRandomColor() {
		return color.get(random.nextInt(color.size()));
	}

	private static List<Animal> getAnimalList() {
		List<Animal> animals = new ArrayList<Animal>();
		for (int i = 0; i < 30; i++) {
			animals.add(new Animal(getRandomType(), getRandomWeight(), getRandomColor(), i));
		}
		return animals;
	}
}
