import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/* Convert the Binary Strings each to Decimal and print the result.
* If Binary String cannot be converted to Decimal, print a message.
*/
class Solution {
	public static int toInteger(String binary) {
		long decimal = 0;
		for (int i = binary.length() - 1; i >= 0; i--) {
			if (binary.charAt(i) == '1') {
				decimal += Math.pow(2, (binary.length() - i - 1));
			}
		}
		if (decimal != 0) {
			return (int) decimal;
		}
		System.out.println("Can't convert this binary string!");
		return 0;
	}
	public static void main(String[] args) {
		String binaryOne = "1011010";
		String binaryTwo = "1070110";
		String binaryThree = "1";
		String binaryFour = "10";
		String binaryFive = "110011";
		String binarySix = "7";
		String binarySeven = "11001";
		String binaryEight = "0110";
		String binaryNine = "1001";
		String binaryTen = "1717";
		String binaryEleven = "11000";
		String binaryTwelve = "1011010";
		String binaryThriteen = "1101";
		String binaryFourteen = "1";
		
		List<String> list = Stream.of(binaryOne,binaryTwo,binaryThree,binaryFour,binaryFive,
				binarySix,binarySeven,binaryEight,binaryNine,binaryTen,binaryEleven,binaryTwelve,
				binaryThriteen,binaryFourteen).collect(Collectors.toList());
		for (int i = 0; i < list.size(); i++) 
		{
			System.out.println(toInteger(list.get(i)));
			//System.out.println(Integer.parseInt(list.get(i), 2));
		}
		
	}
}

