import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
1) A method that takes an input of List of Strings and returns a list
of unique strings
Input: "Is it Sunny", " Sunny it is", "Hello World", "Hello World",
"hello world"
Output: " Sunny it is", "hello world",
Rules:
a) two strings are considered the same if the only difference
between the two is the uppercase and lowercase characters.
b) Two strings are considered the same as long as they have the same
words in them. In the above example " Is it Sunny" and " Sunny it is"
are considered the same.
c) return the last seen string if multiple strings are the same. In
the above example "Hello World", "Hello World", "hello world" return
"hello world"
d) The input order needs to be maintained in the output order.
*/
class Solution2 {
	public static void main(String[] args) {
		List<String> a = new ArrayList<>();
		List<String> b = new ArrayList<>();
		List<String> inputs = Stream.of("Is it Sunny", " Sunny it is", "Hello World", "Hello World",
				"hello world")
				.collect(Collectors.toList());
		/*
		 * List<String> outputs = Solution2.getUniqueStrings(inputs); for (String phrase
		 * : outputs) { System.out.println(phrase); }
		 */
		for (int i = 0; i<=inputs.size()-1; i++) {
			for (int j = i; j<=inputs.size()-1; j++) {
				if(i!=j) {
					String t = compare(inputs.get(i), inputs.get(j));
					if(t != null) {
						a.add(t);
					}
				}
			}
		}
		Long l = 1L;
	}

	public static String compare(String str1, String str2) {
		int flag = 0;
		String[] arr1 = str1.trim().split(" ");
		String[] arr2 = str2.trim().split(" ");
		if (arr1.length == arr2.length) {
			for (String string1 : arr1) {
				for (String string2 : arr2) {
					if(string1.equalsIgnoreCase(string2)) {
						flag = flag + 1;
					}
				}
			}
			if(arr1.length == flag) {
				return str2;
			}
		}
		return null;
	}
}